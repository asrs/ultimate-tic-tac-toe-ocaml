FROM alpine:latest

#==============================================================================#
#------------------------------------------------------------------------------#
# INSTALL

RUN touch ~/.profile
RUN echo "source ~/.profile" >> ~/.bashrc
RUN apk --update add git m4 libc-dev gcc pcre-dev make ocaml opam patch && \
	rm -rf /var/cache/apk/*

#==============================================================================#
#------------------------------------------------------------------------------#
# COPY

RUN git clone https://gitlab.com/mnhdrn/ultimate-tic-tac-toe-ocaml /app
WORKDIR /app

RUN opam init --comp=4.06.1 --disable-sandboxing -a -y
RUN eval $(opam env)
RUN opam install -y conf-m4
RUN opam install -y ocamlfind

RUN ls
RUN eval $(opam env) && make

#==============================================================================#
#------------------------------------------------------------------------------#
# COMPILE

CMD ["/app/main.out"]
