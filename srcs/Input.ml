(* Input.ml *)

let isDigit c = match c with | '1' .. '9' -> true | _ -> false

let checkInput str = (isDigit str.[0]) && (str.[1] = ' ') && (isDigit str.[2])

let parseInput str = (
    (int_of_char str.[0]) - (int_of_char '0'),
    (int_of_char str.[2]) - (int_of_char '0'))

let rec getInput str =
    match str with
    | "" ->
            print_endline "Please provide something -> [1-9] [1-9]";
            getInput (read_line ())
    | _ when (String.length str != 3) || (checkInput str) = false ->
            print_endline "Please provide a conform input -> [1-9] [1-9]";
            getInput (read_line ())
   | _ -> parseInput str
